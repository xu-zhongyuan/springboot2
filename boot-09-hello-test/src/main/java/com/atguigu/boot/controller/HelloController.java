package com.atguigu.boot.controller;


import com.atguigu.boot.dao.Student;
import com.atguigu.hello.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class HelloController {

    @Autowired
    HelloService helloService;

    // request: http://127.0.0.1:8080/hello
    @GetMapping("/hello")
    public String sayHello() {
        String s = helloService.sayHello("张三");
        return s;
    }

    //2. path variable 要视图函数上要使用@PathVariable注解
    // request: http://127.0.0.1:8080/hellopath/haha
    @GetMapping("/hellopath/{msg}")
    public String helloPathVariable(@PathVariable(value = "msg") String msg) {
        return "hello ".concat(msg);
    }

    //3. query string
    // request: http://127.0.0.1:8080/hello/querystring?name=lisi&age=18
    @GetMapping("/hello/querystring")
    public String helloQueryString(String name, int age) {
        return "helloQueryString" + name + age;
    }

    @PostMapping("/hello/multidatapost")
    public String helloMultiDataPost(String name, int age) {
        return "multiDataPost".concat(name).concat(String.valueOf(age));
    }

    // 这个时候需要借助一个注解：@RequestBody，加上这个注解后，Springmvc会从请求体中获取数据并进行相应的转换。
    // 后端传的是一个Json
    @PostMapping("/hello/entity")
    public String helloEntity(@RequestParam Student student) {
        return student.toString();
    }

    // 其实get也可以带实体类,不需要用@RequestParam,但前端发送的请求携带的参数为param,也不是Json
    @GetMapping("/hello/entity")
    public String helloEntityGet(Student student) {
        return student.toString();
    }

    //用Map也需要用@RequestParam注解，否则接收不到参数
    @PostMapping("/hello/map")
    public String helloMap(@RequestParam Map student) {
        return student.toString();
    }

    // 前端传入数组，get方式
    @GetMapping("/hello/array")
    public String helloArray(@RequestParam("name") List<String> name){
        return name.toString();
    }

    // 前端传入数组，以Post方式
    @PostMapping("/hello/array")
    public String helloArrayPost(@RequestBody List<String> name){
        return name.toString();
    }
}
