package com.atguigu.admin;

import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.testng.annotations.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.*;




/*@RunWith*/

/**
 * @BootstrapWith(SpringBootTestContextBootstrapper.class)
 * @ExtendWith(SpringExtension.class)
 */
//@SpringBootTest
public class TestNGTest {


    @Autowired
    JdbcTemplate jdbcTemplate;


    @BeforeClass
    void testBeforeClass() {
        System.out.println("test before class");
    }

    @BeforeTest
    void testBeforeTest() {
        System.out.println("test before test");
    }

    @AfterClass
    void testAfterClass() {
        System.out.println("test after class");
    }

    @AfterTest
    void testAfterTest() {
        System.out.println("test after test");
    }




    @Test
    void testSimpleAssertions() {
        int cal = cal(3, 2);
        //相等
        assertEquals(5, cal, "业务逻辑计算失败");
        Object obj1 = new Object();
        Object obj2 = new Object();
        assertSame(obj1, obj2, "两个对象不一样");

    }

    public int cal(int a1, int a2) {
        return a1 + a2;
    }


    @Test
//    @Ignore
    void testOkhttpForPost() throws IOException {
        String url = "https://wwww.baidu.com";

        /**
         * 发送Post请求
         */
        //1、创建OkHttpClient对象
        OkHttpClient client = new OkHttpClient();
        //2、构建RequestBody
        MediaType type = MediaType.parse("application/x-www-form-urlencode");
        RequestBody body = RequestBody.create(type, "username=1234&password=1234");
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = client.newCall(request).execute();

        System.out.println(response.code());
        System.out.println(response.headers());
        System.out.println(response.body().string());
    }

    @Test
//    @Ignore
    void testOkHttpSimpleGet() throws Exception {
        String url = "https://www.baidu.com";
        //1、创建OkHttpClient
        OkHttpClient client = new OkHttpClient();
        //2、构建request
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        //使用client发送请求，返回一个响应
        Response response = null;
        try {
             response = client.newCall(request).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println(response.code());
        System.out.println(response.headers());
        System.out.println(response.body().string());
    }


}
